/*
Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
В мовах програмування є спецсимволи за участю яких відбувається написання коду. Але, є випадки коли ці ж спецсимволи можуть використовуватись в коді, як звичайні символи. Саме в цьому випадку нам знадобиться екранування.
Які засоби оголошення функцій ви знаєте?
Є два способи: за допомогою аргументів, в них результат повертається з допомогою виклику інших функцій та анонімно з присвоєнням в якості значеня змінних та властивостей об'єктів.   
Що таке hoisting, як він працює для змінних та функцій?
Це механізм в js, в якому змінні та функції переміщаються в області іхної видимості, ще до того як код спрацює. Наприклад: з допомогою цього механізму ми можемо визвати функцію ще до того як вона написана в коді.
*/


/*
Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:

При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.


Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта
*/


"use strict";

function createNewUser() {
    let firstName = prompt('Enter your name.');
    let lastName = prompt('Enter your last name.');
    let birthday = prompt('Enter your birthday (dd.mm.yyyy)', '');
    
    
    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
        },
        setFirstName(name) {
            Object.defineProperty(this, 'firstName', {value: name });
        },
        setLasttName(name) {
            Object.defineProperty(this, 'lastName', {value: name });
        },
        
        getAge() {
            let today = new Date();
            let day = this.birthday.slice(0, 2);
            let month = this.birthday.slice(3, 5);
            let year = this.birthday.slice(6);
            let age = today.getFullYear() - year;
            
            if (month > today.getMonth() || (month === today.getMonth() && day > today.getDay())) {
                age--;
            }
            
            return age;
        },
    };

    Object.defineProperties(newUser, {
        firstName: { writable: false },
        lastName: { writable: false },
    })

    return newUser;


}

const userObj = createNewUser();
console.log(userObj);
console.log(userObj.getAge());
console.log(userObj.getLogin());